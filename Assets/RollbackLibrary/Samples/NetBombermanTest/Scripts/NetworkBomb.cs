using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using Unity.Netcode;
using UnityEngine.UI;
using TMPro;

public class NetworkBomb : SimpleState
{
    [SerializeField] fix explosionTime;

    [Header("Prefabs")] [SerializeField] GameObject explosionPrefab;

    [SerializeField] fix remainingTime;
    [SerializeField]    TextMeshProUGUI text;
    [SerializeField]   private List<NetworkBombStruct> states = new List<NetworkBombStruct>();
    [SerializeField]  private bool willDestroy = false;
    [SerializeField] public OnSpawnAssigToPlayerGameObject onSpawnAssigToPlayerGameObject;
    [SerializeField] private bool status = true;
    private void Awake()
    {
        remainingTime = new fix((int)explosionTime);
        text = GetComponentInChildren<TextMeshProUGUI>();
    }


    private void FixedUpdate()
    {
   //     remainingTime -= new fix( (int)Time.fixedTimeAsDouble);
       remainingTime--;
        if ((long)remainingTime < 0f)
        {
            InstantiateExplosionEffect();
        }
        text.text = "" + remainingTime;
    }

    public override void SaveState(int frameToSave)
    {
        states.Add(new NetworkBombStruct(this,frameToSave));
        if (states.Count>7)
        {
            if ((states[0].state.frameNumber +6)<=frameToSave && states[0].willDestroy)
            {
        //        ReallySimpleRollbackLogic.instance.players[onSpawnAssigToPlayerGameObject.id].objectsWithAutority.Remove(this.gameObject);
            //    ReallySimpleRollbackLogic.instance.players[onSpawnAssigToPlayerGameObject.id].simpleStates.Remove(this); 

                Destroy(this.gameObject);
                willDestroy = false;
            }
            states.RemoveAt(0);
        }
    }

    public override void LoadState(int frameToLoad)
    {
        
        NetworkBombStruct networkBombStruct=    ( states.Find(s => s.state.frameNumber.Equals(frameToLoad)));
        if (networkBombStruct.state.frameNumber!=frameToLoad)
        {
            Debug.LogError("FRame" + frameToLoad + "can be loaded because doesn't exist");
            return;
        }

        explosionTime = networkBombStruct.explosionTime;
        remainingTime = networkBombStruct.remainingTime;
        willDestroy = networkBombStruct.willDestroy;
        status = networkBombStruct.status;
        gameObject.SetActive(status);
    }
 
  void InstantiateExplosionEffect()
  {
      GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
      willDestroy = true;
      status = false;
      gameObject.SetActive(status);
  }
[Serializable]
 public struct  NetworkBombStruct
 {
     public NetworkBombStruct(NetworkBomb bomb,int savedFrame)
     {
         state = new SimpleStateStruct();
         state.frameNumber = savedFrame;
         explosionTime = bomb.explosionTime;
         remainingTime = bomb.remainingTime;
         willDestroy = bomb.willDestroy;
         status = bomb.status;
     }

     public SimpleStateStruct state;
     public fix explosionTime;
     public fix remainingTime;
     public bool willDestroy;
     public bool status;
 }
}