using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Netcode;
using Cinemachine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : SimpleState
{
    [SerializeField] private OnSpawnAssigToPlayerGameObject onSpawnAssigToPlayerGameObject;
    [SerializeField] InputActionAsset inputActions;
    [SerializeField] fix torqueForce=100;
    [Header("Bomb settings")]
    [SerializeField] fix timeBetweenBombs;
    
    [SerializeField]  GameObject networkBombPrefab;
     fix3 moveDirection;
     Rigidbody rb;
    NetworkObject networkObject;
     float timeSinceLastBomb; 
    // ISimpleState<PlayerMovementStruct> thisSimpleState;
    private List<PlayerMovementStruct> states=new List<PlayerMovementStruct>();
    public bool addImpulseOnStart = true;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        if (addImpulseOnStart)
        {
            rb.AddForce((new Vector3(100,100,100)) ,ForceMode.Impulse );
        }
        networkObject = GetComponent<NetworkObject>();
        if (!networkObject) networkObject = GetComponentInParent<NetworkObject>();

        if (!networkObject || networkObject.IsOwner)
        {
            PlayerInput playerInput = gameObject.AddComponent<PlayerInput>();
            playerInput.actions = inputActions;
            playerInput.actions.Enable();
        }

        timeSinceLastBomb = Time.time;
     }
    public void OnMovePlayer(InputValue ctx){}

    void OnMovePlayer(InputActionExtended value)
    {
        
            object unProcessedValue = value.ReadValue();
            if (unProcessedValue != null)
            {
                Vector2 movement = (Vector2)unProcessedValue;

                moveDirection = Vector3.ProjectOnPlane(Camera.main.transform.TransformDirection(-movement), Vector3.up)
                    .normalized.ToFixVec();
                this.moveDirection = moveDirection;
            }
    }

    void OnFire()
    {
        if (  ((Time.time - (long)timeSinceLastBomb) > (long)timeBetweenBombs))
        {
            timeSinceLastBomb = Time.time;
          GameObject g=  Instantiate(networkBombPrefab, transform.position, Quaternion.identity);
          OnSpawnAssigToPlayerGameObject onSpawnAssigToPlayerGameObject=  g.AddComponent<OnSpawnAssigToPlayerGameObject>();
          onSpawnAssigToPlayerGameObject.id = this.onSpawnAssigToPlayerGameObject.id;
        }
    }

    private void FixedUpdate()
    {
        rb.AddTorque(Vector3.Cross(moveDirection.ToUnityVec(), Vector3.up) * (long)torqueForce, ForceMode.Force);
      }
    public override void SaveState(int frameToSave)
    {
        return;
      states.Add(new PlayerMovementStruct(this,frameToSave));
       
//        CreateState(this);
     }

    public override void LoadState(int frameToLoad)
    {
        return;
        PlayerMovementStruct playerMovementStructÌnThisFrame=    ( states.Find(s => s.stateStruct.frameNumber.Equals(frameToLoad)));
        if (playerMovementStructÌnThisFrame.stateStruct.frameNumber!=frameToLoad)
        {
            Debug.LogError("FRame" + frameToLoad + "can be loaded because doesn't exist");
            return;
        }
        this.rb = playerMovementStructÌnThisFrame.rb;
        this.inputActions = playerMovementStructÌnThisFrame.inputActions;
        this.moveDirection = playerMovementStructÌnThisFrame.moveDirection;
        this.networkObject = playerMovementStructÌnThisFrame.networkObject;
        torqueForce = playerMovementStructÌnThisFrame.torqueForce;
         this.transform.position = playerMovementStructÌnThisFrame.transformPosition.ToUnityVec();
        this.transform.rotation = playerMovementStructÌnThisFrame.transformRotation.ToUnityQuat();
        rb.velocity = playerMovementStructÌnThisFrame.rigidbodyVelocity.ToUnityVec();
        //rb.position = playerMovementStructÌnThisFrame.rigidbodyPosition.ToUnityVec();
        //rb.rotation = playerMovementStructÌnThisFrame.rigidbodyRotation.ToUnityQuat();
        rb.angularVelocity = playerMovementStructÌnThisFrame.rigidbodyAngularVelocity.ToUnityVec();
        timeBetweenBombs = playerMovementStructÌnThisFrame.timeBetweenBombs;
        networkBombPrefab = playerMovementStructÌnThisFrame.networkBombPrefab;
        timeSinceLastBomb = playerMovementStructÌnThisFrame.timeSinceLastBomb;
      }
    [System.Serializable]
    public struct PlayerMovementStruct{
        [SerializeField] public InputActionAsset inputActions;
        [SerializeField] public fix torqueForce;
        [SerializeField] public fix timeBetweenBombs;
        [SerializeField] public GameObject networkBombPrefab;
        [SerializeField] public fix3 moveDirection;
        [SerializeField] public  Rigidbody rb;
        [SerializeField] public  NetworkObject networkObject;
        [SerializeField] public  float timeSinceLastBomb;
        [SerializeField] public SimpleStateStruct stateStruct;
        [SerializeField] public fix3 rigidbodyAngularVelocity;
        [SerializeField] public fix3 rigidbodyVelocity;
         [SerializeField] public fix3 rigidbodyPosition;
        [SerializeField] public fixQuaternion rigidbodyRotation;
        [SerializeField] public fix3 transformPosition;
        [SerializeField] public fixQuaternion transformRotation;

        public PlayerMovementStruct(PlayerMovement playerMovement,int frameToSave)
        {
            inputActions = playerMovement.inputActions;
            stateStruct = new SimpleStateStruct();
            stateStruct.frameNumber = frameToSave;
            torqueForce = playerMovement.torqueForce;
            timeBetweenBombs = playerMovement.timeBetweenBombs;
            networkBombPrefab = playerMovement.networkBombPrefab;
            moveDirection = playerMovement.moveDirection;
            rb = playerMovement.rb;
            networkObject = playerMovement.networkObject;
            timeSinceLastBomb = playerMovement.timeSinceLastBomb;
            rigidbodyVelocity = playerMovement.rb.velocity.ToFixVec();
            rigidbodyAngularVelocity = playerMovement.rb.angularVelocity.ToFixVec();
            transformPosition = playerMovement.transform.position.ToFixVec();
            transformRotation = playerMovement.transform.rotation.ToFixQuat();
            rigidbodyPosition =  playerMovement.rb.position.ToFixVec();
            rigidbodyRotation =  playerMovement.rb.rotation.ToFixQuat();
            

            //    torqueForce=
        }
    
    }

  
}
