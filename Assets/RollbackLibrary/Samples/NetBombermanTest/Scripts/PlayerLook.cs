using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Netcode;
using Cinemachine;
using TMPro.SpriteAssetUtilities;
using UnityEditor;

public class PlayerLook : SimpleState
{
    CinemachineVirtualCameraBase vCam;
    [SerializeField] fix lookSpeedX = 100;
    [SerializeField] fix lookSpeedY = 1;
    CinemachineFreeLook freeLook;
    private List<PlayerLookStruct> states= new List<PlayerLookStruct>();

    private void Start()
    {
        NetworkObject networkObject = GetComponent<NetworkObject>();
        if (!networkObject) networkObject = GetComponentInParent<NetworkObject>();
        vCam = GetComponentInChildren<CinemachineVirtualCameraBase>();
        freeLook = GetComponentInChildren<CinemachineFreeLook>();

        if (networkObject && !networkObject.IsOwner)
        {
            vCam.enabled = false;
        }
    }

    fix2 lookMovement = Vector2.zero.ToFixVec();
    void OnLook(InputActionExtended value)
    {
        //Normalize the vector to have an uniform vector in whichever form it came from (I.E Gamepad, mouse, etc)
        object unProcessedValue = value.ReadValue();
        if (unProcessedValue != null)
        {
            lookMovement = ((Vector2)unProcessedValue).ToFixVec();
        }
    }
    

    private void FixedUpdate()
    {
        //Ajust axis values using look speed and Time.deltaTime so the look doesn't go faster if there is more FPS
        freeLook.m_XAxis.Value +=  (long)lookMovement.x * (long)lookSpeedX * Time.deltaTime;
        freeLook.m_YAxis.Value +=  (long)lookMovement.y * (long)lookSpeedY * Time.deltaTime;
    }


   public override void SaveState(int frameToSave)
   {
       states.Add(new PlayerLookStruct(this,frameToSave));
       
   }

   public override void LoadState(int frameToLoad)
   {
       PlayerLookStruct playerMovementStructÌnThisFrame=    ( states.Find(s => s.stateStruct.frameNumber.Equals(frameToLoad)));
       if (playerMovementStructÌnThisFrame.stateStruct.frameNumber!=frameToLoad)
       {
           Debug.LogError("FRame" + frameToLoad + "can be loaded because doesn't exist");
           return;
       }

       lookSpeedX = playerMovementStructÌnThisFrame.lookSpeedX;
       lookSpeedY = playerMovementStructÌnThisFrame.lookSpeedY;
       lookMovement = playerMovementStructÌnThisFrame.lookMovement;
   }
   [Serializable]
   public struct PlayerLookStruct
   {
       [SerializeField]public fix lookSpeedX;
       [SerializeField]public fix lookSpeedY;
       [SerializeField]public fix2 lookMovement;
       [SerializeField] public SimpleStateStruct stateStruct;
        
       public PlayerLookStruct(PlayerLook playerLook, int frame)
       {
           stateStruct = new SimpleStateStruct();
           stateStruct.frameNumber = frame;
           lookSpeedX= playerLook.lookSpeedX;
           lookSpeedY= playerLook.lookSpeedY;
           lookMovement = playerLook.lookMovement;
       }

   }
}
