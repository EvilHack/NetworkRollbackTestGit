using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CinemachineFreeLook))]
public class FreeLookAddOn : SimpleState
{
    [Range(0f, 10f)] public fix LookSpeed = 1;
    public bool InvertY = false;
    private CinemachineFreeLook _freeLookComponent;
    private List<FreeLookAddOnStruct> states=new List<FreeLookAddOnStruct>();
    private fix2 lookMovement;
    public void Start()
    {
        _freeLookComponent = GetComponent<CinemachineFreeLook>();
    }

    // Update the look movement each time the event is trigger
    public void OnLook(InputActionExtended context)
    {
        //Normalize the vector to have an uniform vector in whichever form it came from (I.E Gamepad, mouse, etc)
     
        object unProcessedValue = context.ReadValue();
        if (unProcessedValue != null)
        {
              lookMovement = ((Vector2)unProcessedValue).normalized.ToFixVec();
            lookMovement.y = InvertY ? -lookMovement.y : lookMovement.y;

            // This is because X axis is only contains between -180 and 180 instead of 0 and 1 like the Y axis
            lookMovement  =  new fix2(lookMovement.x * 180,lookMovement.y );
            //Ajust axis values using look speed and Time.deltaTime so the look doesn't go faster if there is more FPS
            _freeLookComponent.m_XAxis.Value += (float)lookMovement.x * (float)LookSpeed * Time.deltaTime;
            _freeLookComponent.m_YAxis.Value += (float)lookMovement.y * (float)LookSpeed * Time.deltaTime;
        }
    }

    public override void LoadState(int frameToLoad)
    {
        FreeLookAddOnStruct freeLookAddOnStruct=    ( states.Find(s => s.stateStruct.frameNumber.Equals(frameToLoad)));
        if (freeLookAddOnStruct.stateStruct.frameNumber!=frameToLoad)
        {
            Debug.LogError("FRame" + frameToLoad + "can be loaded because doesn't exist");
            return;
        }
        LookSpeed = freeLookAddOnStruct.LookSpeed;
        lookMovement = freeLookAddOnStruct.lookMovement;
        
    }  
    public override void SaveState(int frameToSave)
    {
        states.Add(new FreeLookAddOnStruct(this,frameToSave));
    }
    
   [Serializable] public struct FreeLookAddOnStruct
    {
        [SerializeField] public SimpleStateStruct stateStruct;
        [SerializeField]public fix LookSpeed;
         [SerializeField]public fix2 lookMovement;


        public FreeLookAddOnStruct(FreeLookAddOn addon,int frame)
        {
            stateStruct = new SimpleStateStruct();
            stateStruct.frameNumber = frame;
            LookSpeed= addon.LookSpeed;
            lookMovement = addon.lookMovement;
        }
    }
}
