using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CubeTransformMove : SimpleState
{
    [SerializeField]   private List<CubeStateBasic> previousCubeStates= new List<CubeStateBasic>();
    [SerializeField]  private int speed=5;
     [SerializeField] private fix3 moveDirection= new ();

     [SerializeField] private Vector3 debugpos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

     void FixedUpdate()
    {
        Debug.LogError("Final direction " + moveDirection.ToUnityVec()*(long )speed * Time.fixedDeltaTime);
        debugpos = (moveDirection.ToUnityVec() *  speed) + transform.position;
        transform.position = debugpos;
    }
     public void OnMovePlayer(InputValue ctx){}

     public void OnMovePlayer(InputActionExtended inputActionExtended)
     {

         object unProcessedValue = inputActionExtended.ReadValue();
         if (unProcessedValue != null)
         {
             Vector2 movement = (Vector2)unProcessedValue;
             moveDirection = (new Vector3(movement.x, movement.y).ToFixVec());
             Debug.LogWarning("InpuTEventManuallyworks" + inputActionExtended.phaseOfTheAction);
         }
     }

     public override void SaveState(int frameToSave)
    {
        if (previousCubeStates.Count>100)
        {
            previousCubeStates.RemoveAt(0);
        }
        previousCubeStates.Add(new CubeStateBasic(this,frameToSave));    }

    public override void LoadState(int frameToLoad)
    { 
        CubeStateBasic cubeStateToLoad=    previousCubeStates.Find((state) => state.stateStruct.frameNumber == frameToLoad);
        if (cubeStateToLoad.stateStruct.frameNumber!=frameToLoad)
        {
            Debug.LogError("FRame" + frameToLoad + "can be loaded because doesn't exist");
            return;
        }
          
        this.transform.position = cubeStateToLoad.transformPosition.ToUnityVec();
        this.transform.rotation = cubeStateToLoad.transformRotation.ToUnityQuat();
        this.speed = cubeStateToLoad.speed;

    }
    [Serializable]  public struct CubeStateBasic
    {
        public CubeStateBasic(CubeTransformMove cubeStateManager,int frameNumber)
        {
            stateStruct = new SimpleStateStruct();
            stateStruct.frameNumber = frameNumber;
            speed =  cubeStateManager.speed;
            //isMoving =  cubeStateManager.isMoving;
            moveDirection = cubeStateManager.moveDirection;
            transformPosition = cubeStateManager.transform.position.ToFixVec();
            transformRotation = cubeStateManager.transform.rotation.ToFixQuat();
        
            
        } 
        public SimpleStateStruct stateStruct;
        public int speed;
      //  public bool isMoving;
        public fix3 moveDirection;
        [Header("Transform Saved Settings")]
        public fix3 transformPosition;
        public fixQuaternion transformRotation;
    
   


    }
}
