using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class CubeStateManager : SimpleState
{
    [SerializeField] [Tooltip("Que no sabes leer?")]
    private bool isMoving;

    [SerializeField] private fix speed = 5;
    [SerializeField] private fix torqueForce = new(40);

    [SerializeField] private fix3 moveDirection = new();
    // struct name

    [SerializeField] private Rigidbody rb;
    public bool useStruct = false;

    [SerializeField] private List<CubeState> previousCubeStates = new List<CubeState>();
    public bool addImpulseOnStart = true;

    private void Start()
    {
        if (addImpulseOnStart)
        {
            rb.AddForce((new Vector3(100, 100, 100)), ForceMode.Impulse);
        }
    }

    private void FixedUpdate()
    {
        fix3 appliedForce = (moveDirection * torqueForce);
        rb.AddForce((appliedForce.ToUnityVec()), ForceMode.Force);
    }

    [ContextMenu("savestate")]
    public override void SaveState(int frameToSave)
    {
        if (useStruct)
        {
            CubeState cubeState = new CubeState(this, frameToSave);
            if (previousCubeStates.Count > 100)
            {
                previousCubeStates.RemoveAt(0);
            }

            previousCubeStates.Add(cubeState);
        }
        else
        {
            base.SaveState(frameToSave);
            CubeStateInClass cubeState = new CubeStateInClass(this, frameToSave);
            olderStates.Add(cubeState);
        }


        //   Debug.Log("State saved at frame " +frameToSave);
    }

    [ContextMenu("loadState")]
    public override void LoadState(int frameToLoad)
    {
        if (useStruct)
        {
            CubeState cubeStateToLoad =
                previousCubeStates.Find((state) => state.stateStruct.frameNumber == frameToLoad);
            if (cubeStateToLoad.stateStruct.frameNumber != frameToLoad)
            {
                Debug.LogError("FRame" + frameToLoad + "can be loaded because doesn't exist");
                return;
            }

            this.isMoving = cubeStateToLoad.isMoving;
            this.speed = cubeStateToLoad.speed;
            this.torqueForce = cubeStateToLoad.torqueForce;
            this.transform.position = cubeStateToLoad.transformPosition.ToUnityVec();
            this.transform.rotation = cubeStateToLoad.transformRotation.ToUnityQuat();
            rb.velocity = cubeStateToLoad.rigidbodyVelocity.ToUnityVec();
            rb.angularVelocity = cubeStateToLoad.rigidbodyAngularVelocity.ToUnityVec();
        }
        else
        {
            CubeStateInClass cubeStateToLoad = (CubeStateInClass)GetState(frameToLoad);
            if (cubeStateToLoad == null)
            {
                Debug.LogError("Can't get the state at frame " + frameToLoad);
                return;
            }

            moveDirection = cubeStateToLoad.moveDirection;
            this.isMoving = cubeStateToLoad.isMoving;
            this.speed = cubeStateToLoad.speed;
            this.torqueForce = cubeStateToLoad.torqueForce;
            this.transform.position = cubeStateToLoad.transformPosition.ToUnityVec();
            this.transform.rotation = cubeStateToLoad.transformRotation.ToUnityQuat();
            rb.velocity = cubeStateToLoad.rigidbodyVelocity.ToUnityVec();
            rb.angularVelocity = cubeStateToLoad.rigidbodyAngularVelocity.ToUnityVec();
        }
    }

    public void OnMovePlayer(InputValue ctx)
    {
    }

    public void OnMovePlayer(InputActionExtended inputActionExtended)
    {
        object unProcessedValue = inputActionExtended.ReadValue();
        if (unProcessedValue != null)
        {
            Vector2 movement = (Vector2)unProcessedValue;
            moveDirection = (new Vector3(movement.x, 0, movement.y).ToFixVec());
            Debug.LogWarning("InpuTEventManuallyworks");
        }
        else
        {
            moveDirection = (new Vector3().ToFixVec());
        }
    }


    [Serializable]
    public struct CubeState
    {
        public CubeState(CubeStateManager cubeStateManager, int frameNumber)
        {
            stateStruct = new SimpleStateStruct();
            stateStruct.frameNumber = frameNumber;
            speed = cubeStateManager.speed;
            isMoving = cubeStateManager.isMoving;
            torqueForce = cubeStateManager.torqueForce;
            moveDirection = cubeStateManager.moveDirection;
            transformPosition = cubeStateManager.transform.position.ToFixVec();
            transformRotation = cubeStateManager.transform.rotation.ToFixQuat();
            rigidbodyVelocity = cubeStateManager.rb.velocity.ToFixVec();
            rigidbodyAngularVelocity = cubeStateManager.rb.angularVelocity.ToFixVec();
            rigidbodyPosition = cubeStateManager.rb.position.ToFixVec();
            rigidbodyRotation = cubeStateManager.rb.rotation.ToFixQuat();
        }

        public SimpleStateStruct stateStruct;
        public fix speed;
        public bool isMoving;
        public fix torqueForce;
        public fix3 moveDirection;
        [Header("Transform Saved Settings")] public fix3 transformPosition;
        public fixQuaternion transformRotation;
        [Header("Rigidbody Settings")] public fix3 rigidbodyPosition;
        public fixQuaternion rigidbodyRotation;
        public fix3 rigidbodyVelocity;
        public fix3 rigidbodyAngularVelocity;
    }

    [Serializable]
    public class CubeStateInClass : SimpleStateClass
    {
        public CubeStateInClass(CubeStateManager cubeStateManager, int frameNumber)
        {
            this.frameNumber = frameNumber;
            speed = cubeStateManager.speed;
            isMoving = cubeStateManager.isMoving;
            torqueForce = cubeStateManager.torqueForce;
            moveDirection = cubeStateManager.moveDirection;
            transformPosition = cubeStateManager.transform.position.ToFixVec();
            transformRotation = cubeStateManager.transform.rotation.ToFixQuat();
            rigidbodyVelocity = cubeStateManager.rb.velocity.ToFixVec();
            rigidbodyAngularVelocity = cubeStateManager.rb.angularVelocity.ToFixVec();
            rigidbodyPosition = cubeStateManager.rb.position.ToFixVec();
            rigidbodyRotation = cubeStateManager.rb.rotation.ToFixQuat();
        }

        [SerializeField] public fix speed;

        [SerializeField] public bool isMoving;

        [SerializeField] public fix torqueForce;

        [SerializeField] public fix3 moveDirection;

        [Header("Transform Saved Settings")] [SerializeField]
        public fix3 transformPosition;

        [SerializeField] public fixQuaternion transformRotation;

        [Header("Rigidbody Settings")] [SerializeField]
        public fix3 rigidbodyPosition;

        [SerializeField] public fixQuaternion rigidbodyRotation;

        [SerializeField] public fix3 rigidbodyVelocity;

        [SerializeField] public fix3 rigidbodyAngularVelocity;
    }
}