using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NetRecordedInputs", menuName = "NetRecordedInputs", order = 1)]
public class NetRecordedInputs : ScriptableObject
{
    [SerializeField] public List<PlayerWithId> inputs;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    class  ListOfLis
    {
        
    }

    private void OnValidate()
    {
        inputs.ForEach((input)=> input.thisClientInputsOrdered.ForEach(inputInATick =>inputInATick.hasBeenProcessed=false));
    }
}
