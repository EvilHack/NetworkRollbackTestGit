using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[Serializable]
public class InputActionExtended : ICloneable
{
    [SerializeField] public InputActionProperty property;
    [SerializeField] public bool hasBeenProcessed = false;
    [SerializeField] public InputType inputType;
    [SerializeField] public int frameWereWasPerformed;
    [SerializeField] public InputActionPhase phaseOfTheAction;
    [SerializeField] public object value;

    public InputActionExtended()
    {
    }

    public InputActionExtended(InputAction inputAction, int frameWereWasPerformed, InputActionPhase phase, object value)
    {
        property = new InputActionProperty(inputAction);
        phaseOfTheAction = phase;
        this.value = value;
        Initialize(property, frameWereWasPerformed);
    }


    private void Initialize(InputActionProperty inputActionProperty, int frameWereWasPerformed)
    {
        this.frameWereWasPerformed = frameWereWasPerformed;
        InputAction inputAction = inputActionProperty.action;
        switch (inputAction)
        {
            case var v when v.name == "A" || v.name == "B" || v.name == "C" || v.name == "D":
                inputType = InputType.Button;
                break;
            case var v when v.name == "A1" || v.name == "B1" || v.name == "C1" || v.name == "D1":
                inputType = InputType.Button;
                break;
            default:
                inputType = InputType.Motion;
                break;
        }
    }

    public object ReadValue()
    {
        return value;
    }


    public enum InputType
    {
        Button,
        Motion
    }

    public object Clone()
    {
        return new InputActionExtended(property.action.Clone(), frameWereWasPerformed, phaseOfTheAction, value);
    }
}