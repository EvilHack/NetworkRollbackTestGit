using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class OnSpawnAssigToPlayerGameObject : MonoBehaviour
{
    public int id = 0;

    public bool useNetworkObjectID;

    public NetworkObject networkObject;

    public ReallySimpleRollbackLogic reallySimpleRollback;
    
    // Start is called before the first frame update
    void Start()
    {
        if (networkObject!=null&&useNetworkObjectID)
        {
            id = (int)networkObject.OwnerClientId;
        }
        
        reallySimpleRollback =FindObjectOfType<ReallySimpleRollbackLogic>();
        if (reallySimpleRollback!=null)
        {
          bool founded=  reallySimpleRollback.AssignGameObjectToPlayer(this.gameObject, id);
          if (!founded)
          {
              Invoke(nameof(Start),0.1f); // todo make sure to call AssignGameObjectToPlayer when both players are already in ReallySimpleRollbackLogic
          }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (reallySimpleRollback != null) return;
        reallySimpleRollback = FindObjectOfType<ReallySimpleRollbackLogic>();
        if (reallySimpleRollback == null) return;
        reallySimpleRollback.AssignGameObjectToPlayer(this.gameObject,id);
    }
}
