using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class ReallySimpleRollbackLogic : NetworkBehaviour
{
    public static ReallySimpleRollbackLogic instance;
    public int playerInThisMachine;

    public int MAX_ROLLBACK_FRAMES = 7;
    public int FRAME_ADVANTAGE_LIMIT = 8; // These 2 are const 
    private const int INITIAL_FRAME = 0;

    public int numberOfPlayers;
    public List<PlayerWithId> players = new List<PlayerWithId>();

    public bool isServer;
    public int clientTickNumber;

    //todo multiple remote frame for players>2
    public int remoteFrame; //otheclientFrame   or hostframe  when players>2
    public int localFrame; //localClientFrame
    public int lastSyncFrame;
    public int remoteFrameAdvantage;


    public bool inRollback;
    public int latestReceivedRemoteFrame = 0;
    private bool sentAnInputThisFrame = false;
    public int inputDelay = (int)(1 / 60f) * 100;

    public UnityEvent fixedUpdateEvent;

    [Header("LocalInputs")]
    [SerializeField]
    private InputActionTrace localInputActionTrace;

    [SerializeField] private InputActionAsset inputManager;

    [Header("Debug")]  

    public TMP_Text currentFrameCountText;
    [SerializeField] public List<NetRecordedInputs> recordedInputs;
    public int frameToStartReplay;

    [Header("GameState Settings")] public bool isPaused = false;


    public override void OnNetworkSpawn()
    {
        playerInThisMachine = (int)NetworkManager.Singleton.LocalClient.ClientId;
    }


    private void OnEnable()
    {
        inputManager.Enable();

        localInputActionTrace = new InputActionTrace();
        InputActionMap inputActionMap = inputManager.FindActionMap("Player");
        if (inputActionMap != null) localInputActionTrace.SubscribeTo(inputActionMap);
        else
        {
            Debug.LogError("This inputmap doesn´t exist");
            foreach (var inputAction in inputManager.actionMaps[0].actions)
            {
                localInputActionTrace.SubscribeTo(inputAction);
            }
        }
    }

    private void OnDisable()
    {
        if (NetworkManager.NetworkTickSystem == null)
        {
            return;
        }
        NetworkManager.NetworkTickSystem.Tick -= OnReceiveTick;
    }

    public override void OnDestroy()
    {
        localInputActionTrace.UnsubscribeFromAll();
        localInputActionTrace.Dispose();
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        isServer = IsServer;
        Time.fixedDeltaTime = (int)(1 / RollbackableNetworkManager.instance.frameRate);
        Time.maximumDeltaTime = (int)(1 / RollbackableNetworkManager.instance.frameRate);

        inputDelay = (int)(1 / RollbackableNetworkManager.instance.frameRate) * 100;
    }


    private void Start()
    {
        numberOfPlayers = RollbackableNetworkManager.instance.numberOfPlayers;
        if (players.Count == 0)
        {
            SetupPlayers(numberOfPlayers);
        }

        NetworkManager.NetworkTickSystem.Tick += OnReceiveTick;

        localFrame = INITIAL_FRAME;
        remoteFrame = INITIAL_FRAME;
        lastSyncFrame = INITIAL_FRAME;
        remoteFrameAdvantage = 0;
        if (players.Count == 1)
        {
            FRAME_ADVANTAGE_LIMIT = int.MaxValue;
            MAX_ROLLBACK_FRAMES = int.MaxValue;
        }
    }

    [SerializeField] private bool foundUnsyncInput;

    private void FixedUpdate()
    {
        sentAnInputThisFrame = false;
        if (IsClient)
        {
            int finalFrame;

            #region UpdateNetwork

            remoteFrame = latestReceivedRemoteFrame;
            remoteFrameAdvantage = (localFrame - remoteFrame);

            #endregion


            #region Update Synchronization

            finalFrame =  Mathf.Min(remoteFrame, localFrame);

            
            //If there are more than 2 players sync with the host
            List<ClientInputsInATick> otherPlayerInputs = null;
            if (players.Count > 1)
            {
                int firstFrameToSearch =   remoteFrame;
                if (finalFrame < firstFrameToSearch)
                {
                    otherPlayerInputs = players.Count > 2
                        ? players[0].thisClientInputsOrdered.GetInputsFromFrame(finalFrame, firstFrameToSearch)
                        : GetFirstPlayerThatIsNotThisPlayer().thisClientInputsOrdered
                            .GetInputsFromFrame(finalFrame, firstFrameToSearch);
                }
                else
                {
                    otherPlayerInputs = players.Count > 2
                        ? players[0].thisClientInputsOrdered.GetInputsFromFrame(firstFrameToSearch, finalFrame)
                        : GetFirstPlayerThatIsNotThisPlayer().thisClientInputsOrdered
                            .GetInputsFromFrame(firstFrameToSearch, finalFrame);
                }
            }

            foundUnsyncInput = otherPlayerInputs != null && otherPlayerInputs.Count > 0;

            int foundFrame = foundUnsyncInput ? otherPlayerInputs.First().frameWhereWasPerformed : 0;
            lastSyncFrame =
                foundUnsyncInput
                    ? foundFrame - 1
                    : finalFrame; // sync from the last frame where inputs match 

            if (CanRollback())
            {
                Debug.LogError("RollbackTime");
                inRollback = true;


                players.ForEach(pl => pl.simpleStates.ForEach(state => state.LoadState(lastSyncFrame)));

                for (int currentRollbackFrame = lastSyncFrame + 1;
                     currentRollbackFrame < localFrame;
                     currentRollbackFrame++)
                {
                    foreach (var player in players)
                    {
                        UpdateInputs(player.id, currentRollbackFrame);
                    }

                    Debug.Log(currentRollbackFrame + " frame rollback");
                    UpdateGame(1);
                    StoreGameState(currentRollbackFrame);
                }

                foundUnsyncInput = false;
                inRollback = false;
            }
            else
            {
                inRollback = false;
            }

            #endregion

            if (IsTimeSync())
            {
                localFrame++;

                #region UpdateInput

                GetLocalInputs(1);
                foreach (var player in players)
                {
                    UpdateInputs(player.id, localFrame);
                }

                #endregion

                #region Update Game

                UpdateGame(1);

                #endregion

                #region StoreGameState

                StoreGameState(localFrame);

                #endregion
            }
            else
            {
                Debug.LogError("Time is not sync");
            }
        }

        if (!sentAnInputThisFrame)
            SendInputMadeInThisFrameServerRpc(NetworkManager.LocalClient.ClientId,
                new NetworkInput("NOACTION", InputActionPhase.Waiting, new Vector2(), "NONE",
                    localFrame + inputDelay), localFrame,
                NetworkManager.LocalTime.Tick);
       
    }

    public void SetupPlayers(int numberOfPlayers)
    {
        this.numberOfPlayers = numberOfPlayers;
        if (players == null) players = new List<PlayerWithId>();

        for (int i = 0; i < this.numberOfPlayers; i++)
        {
            players.Add(new PlayerWithId(i, new List<SimpleState>(),
                new List<GameObject>()));
        }

        StartDebugReplay();
    }


    public bool CanRollback()
    {
        return localFrame > lastSyncFrame && ((remoteFrame > lastSyncFrame));

    }


    public bool IsTimeSync()
    {
        int localFrameAdvantatge = localFrame - remoteFrame;
        int frameAdventatgeDifference = localFrameAdvantatge - remoteFrameAdvantage;
        return (localFrameAdvantatge < MAX_ROLLBACK_FRAMES && (
            frameAdventatgeDifference <=
            FRAME_ADVANTAGE_LIMIT));
        //  Only allow the local client to get so far ahead of remote.
    }


    public void GetLocalInputs(int numbersOfFrames = 1)
    {
        for (int i = 0; i < numbersOfFrames; i++)
        {
            //    InputSystem.Update();  
            List<NetworkInput> localInputEvents = new List<NetworkInput>();

            foreach (InputActionTrace.ActionEventPtr eventPtr in localInputActionTrace)
            {
                InputAction action = eventPtr.action;
                NetworkInput networkInput = new NetworkInput(eventPtr.action.name, eventPtr.phase,
                    eventPtr.ReadValueAsObject(), eventPtr.action.expectedControlType, localFrame + inputDelay);
                SendInputMadeInThisFrameServerRpc(NetworkManager.LocalClient.ClientId, networkInput, localFrame,
                    NetworkManager.LocalTime.Tick);
                sentAnInputThisFrame = true;
                Debug.LogWarning("messagename: " + "On" + action.name + " ev" + eventPtr.phase);
                localInputEvents.Add(networkInput);
                if (players.Count == 0)
                {
                    Debug.LogError("There isn't any players in this client");
                    continue;
                }
//                InputActionExtended finalAction = new InputActionExtended(eventPtr.action, localFrame, networkInput.phase, eventPtr.action.ReadValueAsObject());  
//////////////////                players[playerInThisMachine].objectsWithAutority.ForEach(o => o.SendMessage("On" + action.name,finalAction,SendMessageOptions.DontRequireReceiver));
            }

            if (localInputEvents.Count > 0)
            {
                players[playerInThisMachine].thisClientInputsOrdered.Add(new ClientInputsInATick(localInputEvents,
                    localFrame + inputDelay, clientTickNumber,
                    false));
            }

            localInputActionTrace.Clear();
        }
    }

    private int pos;

    private void UpdateInputs(int clientId, int frameToUpdate = -1)
    {
        if (frameToUpdate == -1)
        {
            frameToUpdate = lastSyncFrame + 1;
        }

        ClientInputsInATick clientInputsInATick =
            players[clientId].thisClientInputsOrdered.GetInputsOfFrame(frameToUpdate, out pos);
        if (clientInputsInATick != null)
        {
            foreach (var input in clientInputsInATick.inputs)
            {
                InputAction action = inputManager.FindAction(input.actionName);
                if (action == null) continue;
                InputActionExtended actionExtended = new InputActionExtended(action,
                    clientInputsInATick.frameWhereWasPerformed, input.phase,
                    input.GetNonNullValueAsObject());
                players[clientId].objectsWithAutority
                    .ForEach(o => o.BroadcastMessage("On" + input.actionName, actionExtended,
                        SendMessageOptions.DontRequireReceiver));
            }

            clientInputsInATick.hasBeenProcessed = true;
        }
    }


    [ServerRpc(RequireOwnership = false)]
    private void SendInputMadeInThisFrameServerRpc(ulong clientId, NetworkInput input,
        int clientFrame, int clientTimeTick)
    {
//        Debug.Log(input.ToString());
        ClientRpcParams clientRpcParams = new ClientRpcParams
        {
            Send = new ClientRpcSendParams
            {
                TargetClientIds = NetworkManager.ConnectedClients.Keys.ToList()
                    .FindAll((clientIdToSendRpc) => clientIdToSendRpc != clientId).ToArray()
            }
        };
        SendInputToClientRpc((int)clientId, input, clientFrame, clientTimeTick, clientRpcParams);
        Debug.LogError("Served recieved inputs");
    }

    [ClientRpc]
    private void SendInputToClientRpc(int playerId, NetworkInput input, int clientFrame, int clientTimeTick,
        ClientRpcParams clientRpcParams = default)
    {
        latestReceivedRemoteFrame = clientFrame;
        if (playerId > players.Count)
        {
            return;
        }

        if (players.Count == 0)
        {
            return;
        }

        if (input.actionName.Equals("Look"))
        {
            return;
        }

        ClientInputsInATick inputsInOtherClientFrame = players[playerId].thisClientInputsOrdered
            .GetInputsOfFrame(input.targetFrame, out int position);

        if (input.actionName is null or "NOACTION")
        {
            return;
        }

        if (inputsInOtherClientFrame == null)
        {
            players[playerId].thisClientInputsOrdered.Add(new ClientInputsInATick(new List<NetworkInput>() { input },
                input.targetFrame, clientTimeTick, false));
        }
        else
        {
            inputsInOtherClientFrame.inputs.Add(input);
            players[playerId].thisClientInputsOrdered[position] = inputsInOtherClientFrame;
            if (players[playerId].thisClientInputsOrdered[position].hasBeenProcessed)
            {
                Debug.LogWarning($"AlreadyProcessedWhenReceivingAnInput?");
                players[playerId].thisClientInputsOrdered[position].hasBeenProcessed = false;
            }
        }

        Debug.LogError("client  recieved other client inputs");
        
    }

    private void UpdateGame(int numberOfFramesToUpdate = 1)
    {
        fixedUpdateEvent?.Invoke();
        if (!isPaused)
        {
            Physics.Simulate(Time.fixedDeltaTime * numberOfFramesToUpdate);
            //   Physics.Simulate(Time.fixedDeltaTime * numberOfFramesToUpdate);
            currentFrameCountText.text =
                $"Server Frame: {remoteFrame} /n Local Frame: {localFrame} /n Frame Advantage: {remoteFrameAdvantage} /n Last Sync Frame: {lastSyncFrame}";
        }
    }

    private void StoreGameState(int finalFrame)
    {
        foreach (var player in players)
        {
            player.simpleStates.RemoveAll(item => item == null);
            player.objectsWithAutority.RemoveAll(item => item == null); //todo remove nulls only when is necessary

            player.simpleStates.ForEach(state =>
                state.SaveState(finalFrame)); //todo save state only when a input is performed
        }
    }


    public void TogglePause()
    {
        isPaused = !isPaused;
    }

    private void OnReceiveTick()
    {
        clientTickNumber = NetworkManager.LocalTime.Tick;
    }

    public PlayerWithId GetFirstPlayerThatIsNotThisPlayer()
    {
        return players.FirstOrDefault(player => player.id != playerInThisMachine);
    }

    public void AddClient(ulong id)
    {
        if (players.Any(client => client.id == (int)id))
        {
            return;
        }

        players.Add(new PlayerWithId((int)id, new List<SimpleState>(), new List<GameObject>()));
        StartDebugReplay();
    }

    public void RemoveClient(ulong id)
    {
        players.RemoveAll((client) => client.id == (int)id);
    }


    public bool AssignGameObjectToPlayer(GameObject gameObject, int playerId)
    {
        if (playerId >= players.Count)
        {
            Debug.LogError($"Player id {playerId} doesn't exist");

            return false;
        }

        players[playerId].objectsWithAutority.Add(gameObject);
        SimpleState state = gameObject.GetComponent<SimpleState>();
        if (state != null)
        {
            players[playerId].simpleStates.RemoveAll(item => item == null);

            players[playerId].simpleStates.Add(state);
        }

        return true;
    }

    public void StartDebugReplay()
    {
        if (recordedInputs.Count > 0)
        {
            localInputActionTrace.UnsubscribeFromAll();
            for (var index = 0; index < players.Count; index++)
            {
                players[index].thisClientInputsOrdered = recordedInputs[0].inputs[index].thisClientInputsOrdered;
            }
        }
    }


    [Obsolete]
    private ClientInputsInATick FindFirstFrameWherePredictedInputsAndRemoteInputsDoNotMatch(
        List<ClientInputsInATick> localList, List<ClientInputsInATick> remoteList)
    {
        localList.Sort((input1, input2) => input1.frameWhereWasPerformed.CompareTo(input2.frameWhereWasPerformed));
        remoteList.Sort((input1, input2) => input1.frameWhereWasPerformed.CompareTo(input2.frameWhereWasPerformed));
        if (remoteList.Count < localList.Count)
        {
            return null;
        }
        //for (int i = 0; i < lastSyncFrame+1; i++)

        for (int i = 0; i < localList.Count; i++)
        {
            if (localList[i] == remoteList[i])
            {
                if (i + 1 == localList.Count)
                {
                    return null;
                }

                return localList[i + 1];
            }
        }

        return null;
    }

    public void ForceSceneReload()
    {
        //  RollbackableNetworkManager.instance.LoadScene("lobby");
        RollbackableNetworkManager.instance.LoadScene("cubeSceneTest");
    }
}

public static class Extensions
{
    public static List<ClientInputsInATick> GetInputsFromFrame(this List<ClientInputsInATick> clientInputInATick,
        int frame, int untilFrame = -1)
    {
        if (untilFrame == -1)
        {
            untilFrame = frame;
        }

        return clientInputInATick.FindAll(input =>
            input.frameWhereWasPerformed >= frame && input.frameWhereWasPerformed <= untilFrame &&
            !input.hasBeenProcessed); //todo I don't know if check only unprocessed inputs is a good idea
    }

    public static ClientInputsInATick GetInputsOfFrame(this List<ClientInputsInATick> clientInputInATick,
        int desiredFrame,
        out int positionInTheList)
    {
        positionInTheList = -1;

        for (int i = 0; i < clientInputInATick.Count; i++)
        {
            if (clientInputInATick[i].frameWhereWasPerformed == desiredFrame)
            {
                positionInTheList = i;
                return clientInputInATick[i];
            }
        }

        return null;
    }
}

[Serializable]
public class ClientInputsInATick
{
    [SerializeField] public List<NetworkInput> inputs;
    [SerializeField] public int frameWhereWasPerformed;
    [SerializeField] private int tickNumber;
    [SerializeField] public bool hasBeenProcessed = false;

    public ClientInputsInATick(List<NetworkInput> inputs, int frameWhereWasPerformed, int tickNumber,
        bool hasBeenProcessed)
    {
        this.inputs = inputs;
        this.frameWhereWasPerformed = frameWhereWasPerformed;
        this.tickNumber = tickNumber;
        this.hasBeenProcessed = hasBeenProcessed;
    }
}