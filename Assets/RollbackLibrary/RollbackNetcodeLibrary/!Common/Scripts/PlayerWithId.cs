using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerWithId
{
    [SerializeField] public int id;

    [SerializeField] public List<SimpleState> simpleStates;

    //  [SerializeField] public List<ISimpleState<t> simpleStates;
    [SerializeField] public List<GameObject> objectsWithAutority;
    [SerializeField] public List<ClientInputsInATick> thisClientInputsOrdered = new List<ClientInputsInATick>();

    public PlayerWithId(int id, List<SimpleState> simpleStates, List<GameObject> objectsWithAutority)
    {
        this.id = id;
        this.simpleStates = simpleStates;
        this.objectsWithAutority = objectsWithAutority;
    }
}