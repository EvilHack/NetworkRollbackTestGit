using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine.Networking;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class RollbackableNetworkManager : NetworkManager
{
    private void FixedUpdate()
    {
        
    }

    private static List<ulong> players= new List<ulong>();
    public static int macPlayersNumber=2;
    public   int numberOfPlayers= 2;
    public  float frameRate =60f;

    public static RollbackableNetworkManager instance;
    public NetworkStats networkStats;
    private void Setup() 
    {
        if (RollbackableNetworkManager.instance!=null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

        Application.targetFrameRate = (int)frameRate;
        
        NetworkManager.Singleton.ConnectionApprovalCallback += ApprovalCheck;
        NetworkManager.Singleton.OnClientConnectedCallback += ClientConnected;
        NetworkManager.Singleton.OnClientDisconnectCallback += ClientDisconnect;
        if (IsServer)
        {
         //   numberOfPlayers.Value = Singleton.ConnectedClients.Count;
        }
     }
    void OnDisable()
    {
        //numberOfPlayers.OnValueChanged -= UpdateNumberOfPlayers;
    }
    private void Start()
    {
        //numberOfPlayers.OnValueChanged += UpdateNumberOfPlayers;
        Setup();
    }
     
    private void ClientDisconnect(ulong obj)
    {
        players.Remove(obj);  
        RemovePlayerToNetworkManagerClientClientRPC(obj);
       ReallySimpleRollbackLogic.instance.RemoveClient(obj);
        if (IsHost)
        {
            RecalculatePlayersCountClientRpc(Singleton.ConnectedClients.Count);
        }

    }
[ClientRpc]
    public void RemovePlayerToNetworkManagerClientClientRPC(ulong id)
    {
        players.Remove(id);
    }
[ClientRpc]
    public void AddPlayerToNetworkManagerClientClientRPC(ulong id)
    {
        players.Add(id);
    }
    private void ClientConnected(ulong obj)
    {
     //SpawnManager.NetworkManager.ge  
     Debug.Log($"Connected client {obj}");

     players.Add(obj);
     AddPlayerToNetworkManagerClientClientRPC(obj);
    
     
    }

    [ClientRpc]
    public void RecalculatePlayersCountClientRpc(int newValue)
    {
        numberOfPlayers = newValue;
    }
    
     
    public    void LoadScene(string sceneName)
    {
        Singleton.SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        foreach (var client in NetworkManager.Singleton.ConnectedClients)
        {
            client.Value.PlayerObject.transform.position=Vector3.zero;
        }

        Singleton.SceneManager.OnLoadComplete +=  PassConnectedClientsToSimpleRollbackLogic;



    }

    public void OnLoadComplete()
    {
        if (IsHost)
        {
            RecalculatePlayersCountClientRpc(Singleton.ConnectedClients.Count);
        }
    }
    private   void PassConnectedClientsToSimpleRollbackLogic(ulong clientid, string scenename, LoadSceneMode loadscenemode)
    {
       OnLoadComplete();
        ReallySimpleRollbackLogic reallySimpleRollbackLogic=  GameObject.FindObjectOfType<ReallySimpleRollbackLogic>();
      //  reallySimpleRollbackLogic.SetupPlayers(numberOfPlayers);
    //    players.ForEach(c => reallySimpleRollbackLogic.AddClient(c));
    }

    private void ApprovalCheck(ConnectionApprovalRequest connectionApprovalRequest, ConnectionApprovalResponse connectionApprovalResponse)
    { 
     
    }

#if UNITY_EDITOR
    [MenuItem("NetworkShortCuts/ForceHost")]
#endif 
    public static void ForceHost()
    {
        Singleton.StartHost();
    }
#if UNITY_EDITOR
    [MenuItem("NetworkShortCuts/ForceClient")]
    #endif
    public static void ForceClient()
    {
        Singleton.StartClient();
    }
    
}

[System.Serializable]
public class NetworkInput : INetworkSerializable
{
    [SerializeField]  public string actionName;
    [SerializeField]  public InputActionPhase phase;
    [SerializeField]  public string expectedControlType;
    [SerializeField]  public Vector2 valueAsVector2;
    [SerializeField]  public bool valueAsAButton;
    [SerializeField]  private SupportedControlTypes currentControlType;
    [SerializeField]  public int targetFrame;
    public NetworkInput()
    {
        SetControlType();
    }

    public NetworkInput(string actionName, InputActionPhase phase, object value,string expectedControlType,int targetFrame)
    {
        this.phase = phase;
        this.actionName = actionName;
        this.expectedControlType = expectedControlType;
        this.targetFrame = targetFrame;
        if (expectedControlType=="Vector2")
        {
            currentControlType = SupportedControlTypes.Vector2;
            valueAsVector2 = (Vector2)value;
         //   Debug.LogError("Processed a vector value of" +  value );
            
        }else if (expectedControlType=="Button")
        {
            currentControlType = SupportedControlTypes.Button;
           // Debug.LogError("Processed a button pressed"   );
            valueAsAButton =Convert.ToBoolean(value);
        }
     
    }

    public void SetControlType()
    {
        currentControlType = expectedControlType switch
        {
            "Vector2" => SupportedControlTypes.Vector2,
            "Button" => SupportedControlTypes.Button,
            _ => currentControlType
        };
    }

    public object GetNonNullValueAsObject()
    {
        return currentControlType switch
        {
            SupportedControlTypes.Unsupported => null,
            SupportedControlTypes.Vector2 => valueAsVector2,
            SupportedControlTypes.Button => valueAsAButton,
            _ => throw new ArgumentOutOfRangeException()
        };
    }
    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref phase);
        serializer.SerializeValue(ref actionName); 
        serializer.SerializeValue(ref valueAsVector2);
        serializer.SerializeValue(ref valueAsAButton);
        serializer.SerializeValue(ref expectedControlType);
        serializer.SerializeValue(ref currentControlType);
        serializer.SerializeValue(ref targetFrame);

    }

    enum SupportedControlTypes
    {Unsupported,
     Vector2,
        Button,
        
        
        
    }

    public override string ToString()
    {
        if (actionName.Contains("NONE"))
        {
            return "";
        }
        return $"ActionName {actionName} Phase: {phase} ExpectedControlType: {expectedControlType} valueAsVector2: {valueAsVector2} valueAsButton: {valueAsAButton} currentControlType: {currentControlType} targetFrame: {targetFrame}";
    }
}

 
