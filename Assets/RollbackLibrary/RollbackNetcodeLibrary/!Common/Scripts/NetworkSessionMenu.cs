using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using UnityEngine.UI;
using TMPro;
using Unity.Netcode.Transports.UNET;

public class NetworkSessionMenu : MonoBehaviour
{
    [SerializeField] TMP_InputField mode_TextField;
    [SerializeField] TMP_InputField IP_TextField;
    [SerializeField] TMP_InputField connectPort_TextField;
    [SerializeField] TMP_InputField listenPort_TextField;

    [SerializeField] Transform startHostButton;
    [SerializeField] Transform startServerButton;
    [SerializeField] Transform startClientButton;

    [SerializeField] Transform stopHostButton;
    [SerializeField] Transform stopServerButton;
    [SerializeField] Transform stopClientButton;

    public void StartHost()
    {
        
        NetworkManager.Singleton.StartHost();
    }
    public void StartServer() { NetworkManager.Singleton.StartServer(); }
    public void StartClient() { NetworkManager.Singleton.StartClient(); }

    public void Shutdown() { NetworkManager.Singleton.Shutdown(); }

    private void Awake()
    {
        IP_TextField.text = PlayerPrefs.GetString("ConnectAddress", "127.0.0.1");
        connectPort_TextField.text = PlayerPrefs.GetString("ConnectPort", "7777");
        listenPort_TextField.text = PlayerPrefs.GetString("ServerListenPort", "7777");
    }

    private void Update()
    {
        if (NetworkManager.Singleton.IsHost)
        {
            HideAllButtons();
            stopHostButton.gameObject.SetActive(true);
            mode_TextField.text = "Host";
        }
        else if (NetworkManager.Singleton.IsServer)
        {
            HideAllButtons();
            stopServerButton.gameObject.SetActive(true);
            mode_TextField.text = "Server";
        }
        else if (NetworkManager.Singleton.IsConnectedClient)
        {
            HideAllButtons();
            stopClientButton.gameObject.SetActive(true);
            mode_TextField.text = "Connected Client";
        }
        else if (NetworkManager.Singleton.IsClient)
        {
            HideAllButtons();
            stopClientButton.gameObject.SetActive(true);
            mode_TextField.text = "Client";
        }
        else
        {
            mode_TextField.text = "Unconnected";
            SetActiveStartButtons(true);
            SetActiveStopButtons(false);
        }
    }

    void ShowAllButtons()
    {
        SetActiveStartButtons(true);
        SetActiveStopButtons(true);
    }

    void HideAllButtons()
    {
        SetActiveStartButtons(false);
        SetActiveStopButtons(false);
    }

    void SetActiveStartButtons(bool active)
    {
        startHostButton.gameObject.SetActive(active);
        startServerButton.gameObject.SetActive(active);
        startClientButton.gameObject.SetActive(active);
    }

    void SetActiveStopButtons(bool active)
    {
        stopHostButton.gameObject.SetActive(active);
        stopServerButton.gameObject.SetActive(active);
        stopClientButton.gameObject.SetActive(active);
    }

    public void OnEndEdit()
    {
        UNetTransport transport = NetworkManager.Singleton.GetComponent<UNetTransport>();
        transport.ConnectAddress = IP_TextField.text;
        transport.ConnectPort = int.Parse(connectPort_TextField.text);
        transport.ServerListenPort = int.Parse(listenPort_TextField.text);

        Debug.Log("ConnectAddress: " + transport.ConnectAddress +
                    "; ConnectPort: " + transport.ConnectPort +
                    "; ServerListenPort: " + transport.ServerListenPort);

        PlayerPrefs.SetString("ConnectAddress", IP_TextField.text);
        PlayerPrefs.SetString("ConnectPort", connectPort_TextField.text);
        PlayerPrefs.SetString("ServerListenPort", listenPort_TextField.text);
    }
}
