using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[Serializable]
public  abstract class SimpleState : MonoBehaviour
{
   [SerializeField] protected List<SimpleStateClass> olderStates= new List<SimpleStateClass>();
   [SerializeField] private int frameNumber;
    public int maxStatesToSave = 100;
    public virtual void SaveState(int frameToSave)
    {
     //   if (olderStates.Find((state) => state.frameNumber >= frameNumber) != null)
        {
            olderStates.RemoveAll((state) => state.frameNumber >= frameToSave);
        }
        if (olderStates.Count>maxStatesToSave)
        {
            olderStates.RemoveAt(0);
            
        }
        
    }

    public abstract void LoadState(int frameToLoad);

    public virtual SimpleStateClass GetState(int frameNumber)
    {
        SimpleStateClass foundFrame= olderStates.Find((state) => state.frameNumber == frameNumber);
        if (foundFrame==null || foundFrame.frameNumber!=frameNumber)
        {
            Debug.LogError("FRame" + frameNumber + "can be loaded because doesn't exist",this);
            Debug.Break();
            return null;
        }

        return foundFrame;
    }
    

}
[Serializable]
public struct SimpleStateStruct
{
  
  [SerializeField]  public int frameNumber;
}
[Serializable]
public class SimpleStateClass
{
  [SerializeField]  public int frameNumber;
}


public interface ISimpleState<T>
{
    public List<T> olderStates
    {
        get { return olderStates = new List<T>(); }
        set => olderStates = value;
    }

    public void CreateState( T state)
    {
        olderStates.Add(state);
    }
    public void SaveState(int frameToSave)
    {
    // olderStates.Add(new T());   
    }
    public   void LoadState(int frameToLoad); 
    
}
